# 实时聊天室

#### 介绍
全栈项目
####
#### 软件架构
软件架构说明
#### 前端
vue.js + uni-app
#### 后端框架
Node.js的express + websocket + Mongodb

### 开发工具        下载
Visual Studio      Code https://code.visualstudio.com/  
HBuilder X      https://www.dcloud.io/
### 开发环境
cnpm install express --save  
Mongodb 4.2.4
#### 安装教程
npm insert

#### 启动指令
后端
#### npm start
#### 前端 

### 功能模块  
#### 搜索好友使用
#### 函数防抖与节流
token的身份验证方法
#### cnpm install jsonwebtoken
#### 发送邮箱
cnpm i nodemailer --save  
#### 密码加密Bcryptjs
npm install bcryptjs --save
#### uni-app的图片裁剪组件
#### 图片预览功能
#### 聊天时间间隔
#### multer插件 文件上传
npm install --save multer
#### 页面效果
![输入图片说明](https://images.gitee.com/uploads/images/2020/0603/202309_79ee937e_6513093.jpeg "2020060301.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0609/105035_8fdb64ce_6513093.jpeg "聊天页面.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0603/202411_082d811f_6513093.jpeg "2020060302.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0603/202422_77a1c35a_6513093.jpeg "2020060303.jpg")
####
![输入图片说明](https://images.gitee.com/uploads/images/2020/0603/202431_94dcd7fa_6513093.jpeg "2020060304.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0603/202443_e1f0e1f7_6513093.png "2020060305.png")
