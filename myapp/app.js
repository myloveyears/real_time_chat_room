var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
//引用token
var jwt = require('./dao/jwt');
//引用路由
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var filesRouter = require('./routes/files');

var app = express();

var mongoose = require("mongoose");

//socket.io
var server = app.listen(8082);
var io = require('socket.io').listen(server);
require('./dao/socket')(io);
//此处需要安装  post 请求解析的内容
const bodyParser = require("body-parser");


//设置允许跨域访问该服务.
app.all('*', function(req, res, next) {
  //允许访问ip *为所有
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Credentials", true);
    res.header("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS");
    res.header("X-Powered-By", ' 3.2.1')
        //这段仅仅为了方便返回json而已
    res.header("Content-Type", "application/json;charset=utf-8");
    if(req.method == 'OPTIONS') {
        //让options请求快速返回
        res.sendStatus(200); 
    } else { 
        next(); 
    }
});

app.use(bodyParser.json({limit:'50mb'}));
app.use(bodyParser.urlencoded({ limit:'50mb', extended: true }));
//获取静态文件路径
app.use(express.static(__dirname+'/data'));

//token判断
app.use(function(req,res,next){
  if(typeof(req.body.token) != 'undefined'){
    //处理token匹配
    let token = req.body.token;
    let tokenMatch = jwt.verifyToken(token);
    if(tokenMatch == 1){
      //通过验证
      next();
    }else{
      //验证不成功
      res.send({status:300});
    }
    // console.log('查询token'+tokenMatch);
    // res.send({result:tokenMatch});
    // if(tokenMatch){

    // }
  }else{
    next();
  }
})


var url = "mongodb://localhost:27017/mydb?authSource=admin"; //本地数据库地址
mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true ,useFindAndModify:false}, (err, db) => {
  if (err)
    console.log("连接数据库失败" + err)
  else
    console.log("连接数据库成功")
          //
 
})


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');
var hbs = require('hbs');
const { Socket } = require('dgram');
//注册索引+1的helper
hbs.registerHelper("addOne",function(index){
  //返回+1之后的结果
  return index+1;
});

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/files', filesRouter);
app.use('/users', usersRouter);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
