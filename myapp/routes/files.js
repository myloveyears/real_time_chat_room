var express = require('express');
var router = express.Router();

//引入附件上传插件
var multer = require('multer');
//
var mkdir = require('../views/mkdir');

//控制文件存储
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        //活的路径
        let url = req.body.url;
        mkdir.mkdirs('../data/'+url,err => {
            console.log(err);
        });
        cb(null, './data/'+url)
    },
    filename: function (req, file, cb) {
    let name = req.body.name;
    //正侧匹配后缀名
    let type = file.originalname.replace(/.+\./,".");
    console.log(type);
    cb(null, name+type);
    //file.fieldname + '-' + 
    }
  })
   
var upload = multer({ storage: storage })

router.get('/', function(req, res, next) {
    res.render('index', { title: '大佬' });M
});

//前端文件上传
router.post('/files/upload', upload.array('file', 12), function (req, res, next) {
    //  req.files是`photos`文件的数组
    //  req.body将包含文本字段（如果有）
    //路径
    let url = req.body.url;
    //获取文件名
    let name = req.files[0].filename;
    let imgurl = '/'+url+'/'+name;
    //返回给前端
    res.send(imgurl);

  })

module.exports = router;