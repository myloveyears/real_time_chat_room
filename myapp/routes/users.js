var express = require('express');
var router = express.Router();
var path =require("path")
var dbmodel = require('../models/userSchema');
var User = dbmodel.model('User');
//注册页面
var singup = require('./singup');
//搜索页面
var search = require('./search');
//用户详情页
//搜索页面
var infouser = require('./infouser');
//好友
var friend = require('./friend');
//聊天
var chat = require('./chat');
//首页
var index = require('../views/index');


//引用加密文件
var bcrypt = require('../dao/bcrypt');
var mongoose = require("mongoose");
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017";
//引用邮箱发送方法
var emilserver = require('../bin/emailServe');
var signin = require('./signin');
/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});
//访问html页面
router.get('/login',(req,res)=>{
    res.sendFile(path.join(__dirname,'../views/login.html'));
  })
router.post("/login", function(req, res, next) {
  res.setHeader('Content-Type','application/json;charset=utf-8');
  // 查询指定条件的数据
  MongoClient.connect(url, { useNewUrlParser: true ,useUnifiedTopology: true}, function(err, db) {
    res.setHeader('Content-Type','application/json;charset=utf-8');//utf-8防止乱码
      if (err) throw err;
      var dbo = db.db("mydb");
      //查询条件
      var whereStr = { "username": req.body.user, "password": req.body.password };
      dbo.collection('user').find(whereStr).toArray(function(err, result) {
          if (err) throw err;
          console.log("查询指定条件的数据")
          console.log(result)
          if (result == undefined || result.length <= 0)
              res.send("登陆失败")
          else
              res.send("登陆成功")
          db.close();
      })
  })
})

router.get('/list', (req, res) => {
    // 第一步
    // res.render('users',{ userlist:[{"username":"123","password":"456"}] });
    // 第二步
    User.find((err, result) => {
        console.log(result)
        res.render('users', { list: result });
        // {layout:false}
    })
});
router.post('/userlist', (req, res) => {
  User.find((err, result) => {
      console.log(result)
      res.send(JSON.stringify(result));
      // {layout:false}
  })
});


router.post("/add",(req,res)=>{
    // user.save();
  User.create(req.body,(err,result)=>{
      if(err) throw err
      user.find((err,result)=>{
        if(err) throw err
        res.render('users',{ list:result});
     })
    })
  })
router.post('/delete', (req, res) => {
  User.deleteOne(req.body,(err,result) =>{
    if (err) throw err
    user.find((err,result) => {
      if (err) throw err
      res.render('users',{ list:result});
    })
  })
});
router.post('/update', (req, res) => {
  User.updateOne(req.body,(err,result) =>{
    if (err) throw err
    user.find((err,result) => {
      if (err) throw err
      res.render('users',{ list:result});
    })
  })
});
//邮箱测试
router.post('/mail', (req, res) => {
  let mail = req.body.mail;
  //console.log(mail);
  emilserver.emailSignUp(mail,res);
  res.send(mail);
});
//注册用户
router.post('/singup/add', (req, res) => {
  singup.signUp(req,res);
  
});

//用户或邮箱是否占用判断
router.post('/singup/judge', (req, res) => {
  singup.judgeValue(req,res);
  
});

//登录页面
//登录
router.post('/signin/match', (req, res) => {
  signin.singIn(req,res);
  
});

//token测试
// router.post('/signin/test', (req, res) => {
//   //signin.test(req,res);
//   res.send('这里是正确的token');
// });

//搜索页面
//搜索用户
router.post('/search/user', (req, res) => {
  search.searchUser(req,res);
});
//判断是否为好友
router.post('/search/isFriend', (req, res) => {
  search.isFriend(req,res);
});
//搜索群
router.post('/search/group', (req, res) => {
  search.searchGroup(req,res);
});
//搜索用户
router.post('/search/isingroup', (req, res) => {
  search.ifInGroup(req,res);
});
//用户详情
router.post('/infouser/infouser', (req, res) => {
  infouser.infoUser(req,res);
});
//用户信息修改
router.post('/infouser/infoupdate', (req, res) => {
  infouser.userUpdate(req,res);
});
//好友昵称修改
router.post('/infouser/updatemarkname', (req, res) => {
  infouser.updateMarkName(req,res);
});

//获取好友昵称
router.post('/infouser/getmarkname', (req, res) => {
  infouser.getMarkName(req,res);
});

//申请好友
router.post('/friend/applyfriend', (req, res) => {
  friend.applyFriend(req,res);
});
//申请状态修改
router.post('/friend/updatefriendState', (req, res) => {
  friend.updateFriendState(req,res);
});
//拒绝好友或删除好友
router.post('/friend/deletefriend', (req, res) => {
  friend.deleteFriend(req,res);
});

//首页
//获取好友
router.post('/views/index/getfriend', (req, res) => {
  index.getFriend(req,res);
});
//获取最后一条消息
router.post('/views/index/getlasmsg', (req, res) => {
  index.getLasMsg(req,res);
});
//获取好友未读消息数
router.post('/views/index/unreadmsg', (req, res) => {
  index.unreadMsg(req,res);
});
//好友消息标己读
router.post('/views/index/updatemsg', (req, res) => {
  index.updateMsg(req,res);
});

//获取群
router.post('/views/index/getgroup', (req, res) => {
  index.getGroup(req,res);
});
//获取最后一条群消息
router.post('/views/index/getlasgroupmsg', (req, res) => {
  index.getLasGroupMsg(req,res);
});
//群里消息标己读
router.post('/views/index/updategroupmsg', (req, res) => {
  index.updateGroupMsg(req,res);
});
//聊天页面
//获取最后一条群消息
router.post('/chat/msg', (req, res) => {
  chat.msg(req,res);
});
module.exports = router;
