
module.exports = function(io){
    var users = {}; //socket注册用户

    io.on('connection', (Socket) => {

        //console.log('ok');
        //用户登录注册
        Socket.on('login',(id) => {
            console.log(Socket.id);
            Socket.name = id;
            users[id] = Socket.id;
            Socket.emit('login',Socket.id);
        });

        //用户一对一消息发送
        Socket.on('msg',(msg,fromid,toid) => {
            console.log(msg);
            //发送给对方.to(users[toid]),fromid
            if(users[toid]){
                Socket.to(users[toid]).emit('msg',msg,fromid);
            }
        });

        //用户离开
        Socket.on('disconnecting', () => {
            if(users.hasOwnProperty(Socket.name)){
                delete users[Socket.name];
                console.log(Socket.id+'离开');
            }
            //const rooms = Object.keys(Socket.rooms);
        });

    });
}