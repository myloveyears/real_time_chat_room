var dbmodel = require('../models/userSchema');

//todo导表
//todo用户表
var User = dbmodel.model('User');
//todo好友表
var Friend = dbmodel.model('Friend');
//todo群表
var Group = dbmodel.model('Group');
//todo群成员表
var GroupUser = dbmodel.model('GroupUser');
Message
//todo一对一消息表
var Message = dbmodel.model('Message');
var GroupMess = dbmodel.model('GroupMsg');
//todo引用加密文件
var bcrypt = require('../dao/bcrypt');

//todo引用token
var jwt = require('./jwt');
//todo新建用户
exports.builUser = function(name,mail,pwd,res){
    //密码加密
    let pwa = bcrypt.encryption(pwd);

    let data ={
        name:name,
        email:mail,
        pwd:pwa,
        time:new Date(),

    }

    let user = new User(data);

    user.save(function(err,result){
        if(err){
            res.send({status:500});
        }else{
            res.send({status:200});
        }
    })
}

//?匹配用户表元素个数 countDocuments匹配数字
exports.countUserValue = function(data,type,res){
    let Wherestr = {};
    //Wherestr ={'type':data}
    Wherestr[type] = data;
    
    User.countDocuments(Wherestr,function(err,result){
        if(err){
            res.send({status:500});
        }else{
            res.send({status:200,result});
        }
    })
}

//?用户验证
exports.userMatch = function(data,pwd,res){
    let wherestr = {$or:[{'name':data},{'email':data}]};
    let out = {'name':1,'imgurl':1,'pwd':1}

    
    User.find(wherestr,out,function(err,result){
        if(err){
            res.send({status:500});
        }else{
            if(result == ''){
                res.send({status:400});
            }
            result.map(function(e){
                const pwdMatch = bcrypt.verification(pwd,e.pwd);
                console.log(pwdMatch);
                if(pwdMatch){
                    let token = jwt.generateToken(e._id);
                    console.log(token);
                    let back = {
                        id:e._id,
                        name: e.name,
                        imgurl: e.imgurl,
                        token: token,
                    }
                    res.send({status:200,back});
                }else{
                    res.send({status:400});
                }
            })
        }
    }
    )
}

//?搜索用户
exports.searchUser = function(data,res){
    let wherestr;
    if(data == 'hengxin'){
        wherestr = {};
    }else{
        wherestr = {$or:[{'name':{$regex:data}},{'email':{$regex:data}}]};
    }
    let out = {
        'name':1,
        'email':1,
        'imgurl':1,

    }
    User.find(wherestr,out,function(err,result){
        if(err){
            res.send({status:500});
        }else{
            res.send({status:200,result})
        }
    })
}

//?用户匹配 判断是否为好友
exports.ifFriend = function(uid,fid,res){
    let wherestr = {'userID':uid,'friendID':fid,'state':0}
    Friend.findOne(wherestr,function(err,result){
        if(err){
            res.send({status:500});
        }else{
            if(result){
                //是好友
                res.send({status:200})
            }else{
                //不是好友
                res.send({status:400})
            }
        }
    })
}

//?搜索群
exports.searchGroup = function(data,res){
    let wherestr;
    if(data == 'hengxin'){
        wherestr = {};
    }else{
        wherestr = {'name':{$regex:data}};
    }
    let out = {
        'name':1,
        'imgurl':1,

    }
    Group.find(wherestr,out,function(err,res){
        if(err){
            res.send({status:500});
        }else{
            res.send({status:200,result})
        }
    })
}

//?用户匹配 判断是否在群内
exports.ifInGroup = function(uid,gid,res){
    let wherestr = {'userID':uid,'groupID':gid}
    GroupUser.findOne(wherestr,function(err,result){
        if(err){
            res.send({status:500});
        }else{
            if(result){
                //是在群内
                res.send({status:200})
            }else{
                //不在群内
                res.send({status:400})
            }
        }
    })
}

//todo用户详情 userDetial
exports.Infouser = function(id,res){
    let wherestr = {'_id':id};
    let out = {'pwd':0};
    User.findOne(wherestr,out,function(err,result){
        if(err){
            res.send({status:500});
        }else{
            res.send({status:200,result});
        }
    })
}

//todo用户信息修改

function update(data,update,res){
    User.findByIdAndUpdate(data,update,function(err,resu){
        if(err){
            //修改失败
            res.send({status:500});
        }else{
            //修改成功
            res.send({status:200});
        }
    })
}

exports.userUpdate = function(data,res){
    let updatestr = {};
    //判断是否有密码
    if(typeof(data.pwd)!='undefined'){
        //有密码进行匹配
        
        User.find({'_id':data.id},{'pwd':1},function(err,result){
            if(err){
                res.send({status:500});
            }else{
                if(result == ''){
                    res.send({status:400});
                }
                result.map(function(e){
                    const pwdMatch = bcrypt.verification(data.pwd,e.pwd);
                    console.log(pwdMatch);
                    if(pwdMatch){
                        //密码验证成功
                        //如果为修改密码先加密
                        if(data.type == 'pwd'){
                            //密码加密
                            let password = bcrypt.encryption(data.data);
                            updatestr[data.type] = password;
                            update(data.id,updatestr,res);
                        }else{
                            //邮箱匹配
                            updatestr[data.type] = data.data;
                            User.countDocuments(updatestr,function(err,result){
                                if(err){
                                    res.send({status:500});
                                }else{
                                    //没有匹配项,可以修改
                                    if(result == 0){
                                        update(data.id,updatestr,res);
                                    }else{
                                        //已存在
                                        res.send({status:201});
                                    }
                                }
                            })
                        }
                        //updatestr[data.type] = data.data;
                       
                    }else{
                        //密码匹配失败
                        res.send({status:400});
                    }
                })
            }
        }
        )
    }else if(data.type == 'name'){
        //如果用户名先进行匹配
        updatestr[data.type] = data.data;
        User.countDocuments(updatestr,function(err,result){
            if(err){
                res.send({status:500});
            }else{
                //没有匹配项,可以修改
                if(result == 0){
                    update(data.id,updatestr,res);
                }else{
                    //已存在
                    res.send({status:300});
                }
            }
        })
    }
    else{
        //一般项目修改
        updatestr[data.type] = data.data;
        update(data.id,updatestr,res)
    }
}

//?好友昵称修改
exports.updateMarkName = function(data,res){
    let wherestr = {'userID':data.uid,'friendID':data.fid};
    let updatestr = {'markname':data.name};
    Friend.updateOne(wherestr,updatestr,function(err,result){
        if(err){
            //修改失败
            res.send({status:500});
        }else{
            //修改成功
            res.send({status:200});
        }
    })
}
//?获取好友昵称
exports.getMarkName = function(data,res){
    let wherestr = {'userID':data.uid,'friendID':data.fid};
    let out = {'markname':1};
    Friend.findOne(wherestr,out,function(err,result){
        if(err){
            //获取失败
            res.send({status:500});
        }else{
            //获取成功
            res.send({status:200,result});
        }
    })
}

//todo好友操作
//?添加好友表
exports.builFriend = function(uid,fid,state,res){
    let data ={
        userID:uid,
        friendID:fid,
        state:state,
        time:new Date(),
        lastTime:new Date(),
    }

    let friend = new Friend(data);

    friend.save(function(err,result){
        if(err){
            console('申请好友表出错');
        }else{
            //res.send({status:200});
        }
    })
}

//?好友最后通讯时间
exports.upFriendLastTime = function(data){
    let wherestr = {$or:[{'userID':data.uid,'friendID':data.fid},{'userID':data.fid,'friendID':data.uid}]};
    let updatestr = {'lastTime':new Date()};

    Friend.updateMany(wherestr,updatestr,function(err,result){
        if(err){
            console('更新好友最后通讯时间出错');
        }else{
            //res.send({status:200});
        }
    })
}

//?添加一对一消息
exports.insertMsg = function(uid,fid,msg,type,res){
    let data ={
        userID:uid,                    //用户id
        friendID:fid,                  //好友id
        message:msg,                   //内容
        types:type,                    //内容状态 (0文字,1图片连接,3音频连接等等)
        time:new Date(),               //发送时间
        state:1,                       //消息状态(0已读,1未读)
    }

    let message = new Message(data);

    message.save(function(err,result){
        if(err){
            res.send({status:500});
        }else{
            res.send({status:200});
        }
    })
}
//?好友申请
exports.applyFriend = function(data,res){
    //判断是否已经申请过
    let wherestr = {'userID':data.uid,'friendID':data.fid};
    Friend.countDocuments(wherestr,(err,result) => {
        if(err){
            res.send({status:500});
        }else{
            //如果 result = 0 为初次申请
            if(result == 0){
                this.builFriend(data.uid,data.fid,2);
                this.builFriend(data.fid,data.uid,1);
            }else{
                //已经申请过好友
                this.upFriendLastTime(data);
            }
            //添加消息
            this.insertMsg(data.uid,data.fid,data.msg,0,res);
        }
    })
}

//?更新好友状态
exports.updateFriendState = function(data,res){
    let wherestr = {$or:[{'userID':data.uid,'friendID':data.fid},{'userID':data.fid,'friendID':data.uid}]};

    Friend.updateMany(wherestr,{'state':0},function(err,result){
        if(err){
            res.send({status:500});
        }else{
            res.send({status:200});
        }
    })
}

//?拒绝好友或删除好友
exports.deleteFriend = function(data,res){
    
    let wherestr = {$or:[{'userID':data.uid,'friendID':data.fid},{'userID':data.fid,'friendID':data.uid}]};
    
    Friend.deleteMany(wherestr,function(err,result){
        if(err){
            res.send({status:500});
        }else{
            res.send({status:200});
        }
    })
}

//?按要求获取用户列表
exports.getUsers =  function(data,res){
    let query = Friend.find({});
    //查询条件
    query.where({'userID':data.uid,'state':data.state});
    //查找friendID 关联的user对象
    query.populate('friendID');
    //排序方式 最有通讯时间倒序排列
    query.sort({'lastTime':-1})
    //查询结果
    query.exec().then(function(e){
        let result = e.map(function(ver){
            return {
                id:ver.friendID._id,
                name:ver.friendID.name,
                markname:ver.markname,
                imgurl:ver.friendID.imgurl,
                lastTime:ver.lastTime,
                type : 0,
            }
        })
        res.send({status:200,result});
    }).catch(function(err){
        res.send({status:500});
    })
}

//?按要求获取一条一对一消息
exports.getOneMsg = function(data,res){
    let query = Message.findOne({});
    //查询条件
    query.where({$or:[{'userID':data.uid,'friendID':data.fid},{'userID':data.fid,'friendID':data.uid}]});
    //排序方式 最有通讯时间倒序排列
    query.sort({'time':-1})
    //查询结果
    query.exec().then(function(ver){
            let result = {
                message:ver.message,
                time:ver.time,
                types:ver.types,
            }
        res.send({status:200,result});
    }).catch(function(err){
        res.send({status:500});
    })
}

//?汇总一对一消息未读数
exports.unreadMsg = function(data,res){
    //汇总条件
    let wherestr = {'userID':data.uid,'friendID':data.fid,'state':1};
    Message.countDocuments(wherestr,(err,result) => {
        if(err){
            res.send({status:500});
        }else{
           res.send({status:200,result});
        }
    })
}

//?一对一消息状态修改
exports.updateMsg = function(data,res){
    //修改项条件
    let wherestr = {'userID':data.uid,'friendID':data.fid,'state':1};
    //修改内容
    let updatestr = {'state':0};

    Message.updateMany(wherestr,updatestr,(err,result) => {
        if(err){
            res.send({status:500});
        }else{
           res.send({status:200});
        }
    })
}
//!未验证
//按要求获取群列表
exports.getGroup = function(uid,res){
    //!id 为用户,所在的群
    let query = GroupUser.find({});
    //查询条件
    query.where({'userID':uid});
    //查找friendID 关联的user对象
    query.populate('groupID');
    //排序方式 最有通讯时间倒序排列
    query.sort({'lastTime':-1})
    //查询结果
    query.exec().then(function(e){
        let result = e.map(function(ver){
            return {
                id:ver.groupID._id,
                name:ver.groupID.name,
                markname:ver.name,
                imgurl:ver.groupID.imgurl,
                lastTime:ver.lastTime,
                tip : ver.tip,
                type : 1,
            }
        })
        res.send({status:200,result});
    }).catch(function(err){
        res.send({status:500});
    })
}
//!未验证
//按要求获取群消息
exports.getOneGroupMsg = function(gid,res){
    let query = GroupMess.findOne({});
    //查询条件
    query.where({'groupID':gid});
    //关联的user对象
    query.populate('userID');
    //排序方式 最有通讯时间倒序排列
    query.sort({'time':-1})
    //查询结果
    query.exec().then(function(ver){
            let result = {
                message:ver.message,
                time:ver.time,
                types:ver.types,
                name:ver.userId.name,
            }
        res.send({status:200,result});
    }).catch(function(err){
        res.send({status:500});
    })
}
//!未验证
//?群消息状态修改
exports.updateGroupMsg = function(data,res){
    //修改项条件
    let wherestr = {'userID':data.uid,'groupID':data.fid};
    //修改内容
    let updatestr = {'tip':0};

    Message.updateOne(wherestr,updatestr,(err,result) => {
        if(err){
            res.send({status:500});
        }else{
           res.send({status:200});
        }
    })
}
//!未验证
//?信息操作
//?分页获取 一对一聊天数据
exports.msg = function(data,res){
    //data:uid,fid,nowPage,pageSize
    var skipNum = data.nowPage*data.ppageSize;//跳过多少条

    //!id 为用户
    let query = Message.find({});
    
    //查询条件
    query.where({$or:[{'userID':data.uid,'friendID':data.fid},{'userID':data.fid,'friendID':data.uid}]});
    //排序方式 最有通讯时间倒序排列
    query.sort({'time':-1})
    //查找friendID 关联的user对象
    query.populate('userID');
    //跳过条数
    query.skip(skipNum);
    //一页条数
    query.limit(data.ppageSize);
    //查询结果
    query.exec().then(function(e){
        let result = e.map(function(ver){
            return {
                id:ver._id,
                message:ver.message,
                types : ver.types,
                time : ver.time,
                fromId : ver.userID._id,
                imgurl:ver.userID.imgurl,
                
            }
        })
        res.send({status:200,result});
    }).catch(function(err){
        res.send({status:500});
    })
}